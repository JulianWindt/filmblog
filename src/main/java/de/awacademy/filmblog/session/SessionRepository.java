package de.awacademy.filmblog.session;

import de.awacademy.filmblog.user.User;
import org.springframework.data.repository.CrudRepository;


import java.time.Instant;
import java.util.Optional;

public interface SessionRepository extends CrudRepository<Session, String> {

    Optional<Session> findByIdAndExpiresAtAfter(String id, Instant expiresAt);

    Optional<Session> findByUser(User user);
}
