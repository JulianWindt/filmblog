package de.awacademy.filmblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilmBlogApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilmBlogApplication.class, args);
    }

}
