package de.awacademy.filmblog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class KommentarService {

    private final KommentarRepository kommentarRepository;

    @Autowired
    public KommentarService(KommentarRepository kommentarRepository) {
        this.kommentarRepository = kommentarRepository;
    }

    public void addKommentar(Kommentar kommentar) {
        kommentarRepository.save(kommentar);
    }

    public void deleteKommentar(int id) {
        kommentarRepository.deleteById(id);
    }

    public List<Kommentar> getKommentare() {
        return this.kommentarRepository.findAll().stream()
                .collect(Collectors.toList());
    }
}
