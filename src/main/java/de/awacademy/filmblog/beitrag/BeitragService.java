package de.awacademy.filmblog.beitrag;

import de.awacademy.filmblog.Kommentar;
import de.awacademy.filmblog.KommentarService;
import de.awacademy.filmblog.beitrag.Beitrag;
import de.awacademy.filmblog.beitrag.BeitragRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class BeitragService {

    private final BeitragRepository beitragRepository;
    private final KommentarService kommentarService;

    @Autowired
    public BeitragService(BeitragRepository beitragRepository, KommentarService kommentarService) {
        this.beitragRepository = beitragRepository;
        this.kommentarService = kommentarService;
    }

    public void addBeitrag(Beitrag beitrag) {
        beitragRepository.save(beitrag);
    }

    public void editBeitrag(Beitrag beitrag){
        beitragRepository.save(beitrag);
    }

    public Beitrag getBeitrag(int beitragID){
        Beitrag beitrag =  beitragRepository.findBeitragById(beitragID);
        return beitrag;
    }



    public List<Beitrag> getBeitraege() {
        return this.beitragRepository.findAll().stream()
                .sorted(Comparator.comparing(Beitrag::getCreatedAt).reversed())
                .collect(Collectors.toList());
    }

    public void deleteBeitrag(int beitragId) {
        List<Kommentar> kommentarListe = kommentarService.getKommentare();
        for (Kommentar kommentar : kommentarListe) {
            if (kommentar.getBeitrag() != null && kommentar.getBeitrag().getId() == beitragId) {
                kommentarService.deleteKommentar(kommentar.getId());
            }
        }
        beitragRepository.deleteById(beitragId);
    }




}
