package de.awacademy.filmblog.beitrag;

import de.awacademy.filmblog.Kommentar;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Beitrag {

    @GeneratedValue
    @Id
    private int id;

    private String author;

    @NotEmpty(message = "Titel darf nicht leer sein")
    private String title;

    @Column(length = 10_000)
    @NotEmpty(message = "Bloginhalt darf nicht leer sein")
    private String message;

    private String picFileSource;

    private LocalDateTime createdAt;

    @OneToMany(mappedBy = "beitrag")
    private List<Kommentar> kommentare;

    public Beitrag() {}

    public Beitrag(String author, String message) {
        this.author = author;
        this.message = message;
        this.createdAt = LocalDateTime.now();
        this.picFileSource = "default.png";
        this.kommentare = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public List<Kommentar> getKommentare() {
        return kommentare;
    }

    public void setKommentare(List<Kommentar> kommentare) {
        this.kommentare = kommentare;
    }

    public String getAnzahlKommentare() {
        if (kommentare.size() == 1) {
            return "1 Kommentar";
        }
        return kommentare.size() + " Kommentare";
    }

    public String getPicFileSource() {
        return picFileSource;
    }

    public void setPicFileSource(String picFileSource) {
        this.picFileSource = picFileSource;
    }
}
