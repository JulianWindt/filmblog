package de.awacademy.filmblog.beitrag;


import de.awacademy.filmblog.beitrag.Beitrag;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BeitragRepository extends CrudRepository <Beitrag, Integer> {

    List<Beitrag> findAll();
    Beitrag findBeitragById(int id);



}
