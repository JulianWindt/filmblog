package de.awacademy.filmblog;

import de.awacademy.filmblog.beitrag.Beitrag;
import de.awacademy.filmblog.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Entity
public class Kommentar {

    @Id
    @GeneratedValue
    private int id;

    @NotEmpty
    private String text;

    @ManyToOne
    private Beitrag beitrag;

    private LocalDateTime createdAt;

    @ManyToOne
    private User createdByUser;

    @Transient
    private int beitragsNr;

    public Kommentar() {
    }

    public int getId() {
        return id;
    }

    public User getCreatedByUser() {
        return createdByUser;
    }

    public String getCreatedByUserName() {
        return (createdByUser == null) ? "Anonymer User" : createdByUser.getUsername();
    }

    public void setCreatedByUser(User createdByUser) {
        this.createdByUser = createdByUser;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public int getBeitragsNr() {
        return beitragsNr;
    }

    public void setBeitragsNr(int beitragsNr) {
        this.beitragsNr = beitragsNr;
    }
}
