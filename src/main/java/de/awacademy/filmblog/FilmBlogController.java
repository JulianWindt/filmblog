package de.awacademy.filmblog;

import de.awacademy.filmblog.beitrag.Beitrag;
import de.awacademy.filmblog.beitrag.BeitragService;
//import de.awacademy.filmblog.session.Session;
//import de.awacademy.filmblog.session.SessionRepository;
import de.awacademy.filmblog.session.Session;
import de.awacademy.filmblog.session.SessionControllerAdvice;
import de.awacademy.filmblog.session.SessionRepository;
import de.awacademy.filmblog.user.*;
import de.awacademy.filmblog.user.User;
import de.awacademy.filmblog.user.UserService;
import org.hibernate.validator.constraints.ModCheck;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
public class FilmBlogController {

    private final BeitragService beitragService;
    private final UserService userService;
    private final KommentarService kommentarService;
    private final UserRepository userRepository;
    private final SessionRepository sessionRepository;

    private final String UPLOAD_DIR = "src/main/resources/static/images/";

    @Autowired
    public FilmBlogController(BeitragService beitragService, KommentarService kommentarService, UserService userService, UserRepository userRepository, SessionRepository sessionRepository) {
        this.beitragService = beitragService;
        this.userService = userService;
        this.kommentarService = kommentarService;
        this.userRepository = userRepository;
        this.sessionRepository = sessionRepository;
    }


    @GetMapping("/")
    public String index(@ModelAttribute("sessionUser") User sessionUser, Model model) {
        Kommentar kommentar = new Kommentar();
        model.addAttribute("kommentar", kommentar);
        model.addAttribute("beitraege", this.beitragService.getBeitraege());

        return "index";
    }

    @PostMapping("/")
    public String submitComment(@Valid @ModelAttribute Kommentar kommentar, @ModelAttribute("sessionUser") User sessionUser, BindingResult bindingResult, Model model) {

        if (!bindingResult.hasErrors()) {
            model.addAttribute("beitraege", this.beitragService.getBeitraege());
            kommentar.setBeitrag(this.beitragService.getBeitraege().get(kommentar.getBeitragsNr()));
            kommentar.setCreatedAt(LocalDateTime.now());
            kommentar.setCreatedByUser(sessionUser);
            this.kommentarService.addKommentar(kommentar);
        }

        return "redirect:/";
    }

    @GetMapping("/beitrag")
    public String showBeitrag(@RequestParam int id, @ModelAttribute("sessionUser") User sessionUser, Model model) {
        Beitrag beitrag = this.beitragService.getBeitrag(id);
        model.addAttribute(beitrag);

        String[] absaetze = beitrag.getMessage().split("\n");
        model.addAttribute("absaetze", absaetze);

        Kommentar kommentar = new Kommentar();
        model.addAttribute(kommentar);
        return "beitrag";
    }

    @PostMapping("/beitrag")
    public String addKommentar(@Valid @ModelAttribute Kommentar kommentar, @ModelAttribute("sessionUser") User sessionUser,
                               BindingResult bindingResult, Model model, @RequestParam int beitragId) {

        if (!bindingResult.hasErrors()) {
            kommentar.setBeitrag(this.beitragService.getBeitrag(beitragId));
            kommentar.setCreatedAt(LocalDateTime.now());
            kommentar.setCreatedByUser(sessionUser);
            this.kommentarService.addKommentar(kommentar);
        }

        return "redirect:/beitrag?id=" + beitragId + "#comment";
    }



    @PostMapping("/deletecom")
    public String deleteComment(@RequestParam int kommentarID, @RequestParam int beitragId, Model model) {
        this.kommentarService.deleteKommentar(kommentarID);
        return "redirect:/beitrag?id=" + beitragId;
    }

    @PostMapping("/deleteBeitrag")
    public String deleteBeitrag(@RequestParam int beitragId) {
        this.beitragService.deleteBeitrag(beitragId);
        return "redirect:/";
    }

    @GetMapping("/admin")
    public String blogEntry(Model model, @ModelAttribute("sessionUser") User sessionUser) {
        Beitrag beitrag = new Beitrag();
        model.addAttribute("beitrag", beitrag);
        return "admin";
    }

    @GetMapping("/registrierung")
    public String userRegistry(Model model) {
        User user = new User();
        model.addAttribute("user", user);
        return "registrierung";
    }

    @GetMapping("/administration")
    public String administration(Model model, @ModelAttribute("sessionUser") User sessionUser) {
        model.addAttribute("users", this.userService.getUsers());
        return "administration";
    }

    @PostMapping("/admin")
    public String blogSubmit(@Valid @ModelAttribute Beitrag beitrag, BindingResult bindingResult, Model model,
                             @ModelAttribute("sessionUser") User sessionUser) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("beitraege", beitragService.getBeitraege());
            return "admin";
        }
        beitrag.setPicFileSource("default.png");
        beitrag.setCreatedAt(LocalDateTime.now());
        beitrag.setAuthor(sessionUser.getUsername());
        this.beitragService.addBeitrag(beitrag);

        return "redirect:/";
    }

    @PostMapping("/registrierung")
    public String userRegistrySubmit(@Valid @ModelAttribute User user, BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "registrierung";
        }

        this.userService.registriereUser(user);
        return "redirect:/";
    }

    @PostMapping("/administration")
    public String administrationSubmit(@RequestParam int userID) {
        this.userService.changeRole(userID);
        return "redirect:/administration";
    }

    // -------
    // Login
    // -------

    User userSuccess;

    @PostMapping("/login")
    public String loginAttempt(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model, HttpServletResponse response){
        Optional<User> optionalUser = userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());

        if (optionalUser.isPresent()) {
            Session session = new Session(optionalUser.get(), Instant.now().plusSeconds(7*24*60*60));
            sessionRepository.save(session);

            Cookie cookie = new Cookie("sessionId", session.getId());
            response.addCookie(cookie);

            return "redirect:/";
        }
        return "login";
    }

    @PostMapping("/logout")
    public String logout(@CookieValue(value="sessionId", defaultValue = "") String sessionId, HttpServletResponse response) {
        System.out.println("lougout");
        Optional<Session> optionalSession = sessionRepository.findByIdAndExpiresAtAfter(sessionId, Instant.now());
        optionalSession.ifPresent(session -> {
            sessionRepository.delete(session);
        });

        Cookie cookie = new Cookie("sessionId", "");
        cookie.setMaxAge(0);
        response.addCookie(cookie);

        return "redirect:/";
    }

    @GetMapping("/login")
    public String login(Model model){
        User user = new User();
        model.addAttribute("user", user);
        return "login";
    }


    @PostMapping("/editBlog")
    public String blogEdit(Model model, @RequestParam int beitragId) {

        Beitrag beitrag = this.beitragService.getBeitrag(beitragId);
        model.addAttribute("beitrag", beitrag);
        return "/editBlog";
    }

    @PostMapping("/editBlog2")
    public String blogEditSubmit(@Valid @ModelAttribute Beitrag beitrag, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            System.out.println("error");
            return "editBlog";
        }else{
            this.beitragService.editBeitrag(beitrag);
            return "redirect:/";
        }
    }

    //Bild upload
    @PostMapping("/upload")
    public String uploadFile(@RequestParam("bildDatei") MultipartFile bildDatei, RedirectAttributes attributes, @Valid @ModelAttribute Beitrag beitrag) {
        //Datei ist leer?

        if (bildDatei.isEmpty()) {
            attributes.addFlashAttribute("message", "Please select a file to upload.");
            return "redirect:/";
        }

        // Name des Bildes
        String bildName = StringUtils.cleanPath(bildDatei.getOriginalFilename());

        // speichern in der Datei
        try {
            Path path = Paths.get(UPLOAD_DIR + bildName);
            Files.copy(bildDatei.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
        beitrag.setPicFileSource(bildName);
        this.beitragService.editBeitrag(beitrag);
        attributes.addFlashAttribute("message", "You successfully uploaded " + bildName + '!');
        return "redirect:/";
    }

    @PostMapping("/deleteUser")
    public String deleteUser(@RequestParam int userID) {

        User user = userRepository.findById(userID);
        Optional<Session> optionalSession = sessionRepository.findByUser(user);

        if (optionalSession.isPresent()) {
            System.out.println(optionalSession.get().getId());
            sessionRepository.delete(optionalSession.get());
        }

        this.userService.deleteUser(userID);
        return "redirect:/administration";
    }
}
