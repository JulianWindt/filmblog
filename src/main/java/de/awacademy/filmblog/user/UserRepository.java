package de.awacademy.filmblog.user;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Integer> {

    List<User> findAll();

    User findById(int id);

    Optional<User> findByUsernameAndPassword(String username, String password);

}
