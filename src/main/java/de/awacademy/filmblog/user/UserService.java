package de.awacademy.filmblog.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.parser.Entity;
import java.util.List;
import java.util.Optional;


@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void registriereUser(User user) {
        userRepository.save(user);
    }

    public void changeRole(int userID) {
        User user = userRepository.findById(userID);
        if (user.getRole() == null || user.getRole().equals("") || user.getRole().isEmpty()) {
            user.setRole("user");
        } else if (user.getRole().equals("user")) {
            user.setRole("admin");
        } else {
            user.setRole("user");
        }
        userRepository.save(user);
    }

    public void deleteUser(int userID) {
        userRepository.deleteById(userID);
    }

    public List<User> getUsers() {
        return this.userRepository.findAll();
    }

}
