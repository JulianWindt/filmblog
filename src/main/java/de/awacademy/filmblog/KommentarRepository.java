package de.awacademy.filmblog;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface KommentarRepository extends CrudRepository<Kommentar, Integer> {

    List<Kommentar> findAll();

}